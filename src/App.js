import React from 'react';
import { Routes, Route } from 'react-router-dom';
import './App.css';

import HomePage from './HomePage';
import NotFoundPage from './components/components-style/NotFoundPage';
import PersonPage from './components/PersonPage';
import Community from './components/Community';

function App() {
  return (
    <Routes>
      <Route path="/" element={<HomePage />} />
      <Route path="/not-found" element={<NotFoundPage />} />
      <Route path="/community" element={<Community urlSegment="" />} />
      <Route path="/community/:id" element={<PersonPage />} />
    </Routes>
  );
}

export default App;
