import logo from '../dist/your-logo-footer.png';

function Footer() {
    return (
        <footer className="app-footer" id="footer">
            <div className="app-footer__logo">
                <img src={logo} alt="Logo" className="app-logo app-logo--small" /> Project
            </div>

            <address className="app-footer__address">
                123 Street,<br />
                Anytown, USA 12345
            </address>

            <a className="app-footer__email" href="mailto:hello@website.com">
                hello@website.com
            </a>

            <p className="app-footer__rights">
                &copy; 2021 Project. All rights reserved
            </p>
      </footer>
    );
}

export default Footer;
