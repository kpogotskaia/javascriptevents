import logo from '../dist/your-logo-here.png';

function MainSection() {
    return (
        <section className="app-section app-section--image-overlay app-section--image-peak">
        <img src={logo} alt="Logo" className="app-logo" />

        <h1 className="app-title">
          Your Headline <br /> Here
        </h1>
        <h2 className="app-subtitle">
          Lorem ipsum dolor sit amet, consectetur <br />adipiscing elit sed do eiusmod.
        </h2>
      </section>
    );
}

export default MainSection;
