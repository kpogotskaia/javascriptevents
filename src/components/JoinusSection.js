import { useState } from 'react';
import { subcribeFormSubmit, unSubcribeFormSubmit } from '../services/joinUs-service';

function JoinusSection() {

const [isSubscribed, setIsSubscribed] = useState(false);
const [isSubbmitting, setIsSubbmitting] = useState(false);

    return (
      <section className="join-program">
        <h1 className="title__join-program">Join our program</h1>
        <p className="description__join-program">
          Sed do eiusmod tempor incididunt ut
          labore et dolore magna aliqua.
        </p>
        <form className="subscribe-form__join-program"
          onSubmit={(event) => {
            if (!isSubbmitting) {
              if (isSubscribed) {
                unSubcribeFormSubmit(event, setIsSubscribed, setIsSubbmitting)
              } else {
                subcribeFormSubmit(event, setIsSubscribed, setIsSubbmitting);
              }
            }
          }}
        >
          {!isSubscribed &&
            <input className="email-input__join-program" name="email" type="email" placeholder='Email'/>
          }
          <button className={`button__join-program ${isSubbmitting ? 'disable' : ''}`} type="submit">
            {isSubscribed ? 'Unsubscribe' : 'Subscribe'}
          </button>
        </form>
      </section>
    );
}

export default JoinusSection;
