import React from "react";
import { useLocation } from "react-router-dom";
import { useEffect, useState } from "react";

import { getPerson } from '../services/service-persons';
import PersonCard from './PersonCard';

export function PersonPage() {

  const location = useLocation();
  const [person, setPerson] = useState(null);


  useEffect(() => {
    getPerson(location.pathname.split('/').reverse()[0]).then(setPerson);
  }, []);

  return(
    <div className="section">
      <h2 className="app-title">
        Big Community of
        People Like You
      </h2>
      <p className="app-subtitle app-section__community-pharagraph">
        We`re proud of our products,
        and we`re really excited
        when we get feedback from our users.
      </p>
      <div className="user-card">
        {person &&
          <PersonCard person={person}/>
        }
      </div>
    </div>
  );
}

export default PersonPage;
