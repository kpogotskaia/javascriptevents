import { Link } from "react-router-dom";

function PersonCards({persons, className, urlSegment}) {
  return (
    <div className={className}>
      {persons.map(person =>
        <div key={person.id} className="person">
          <img className="person-img" alt="person avatar" src={person.avatar}/>
          <h4 className="person-description">{person.id}</h4>
          <Link to={`${urlSegment ? urlSegment + '/' : ''}${person.id}`} className="user-name">
            <div className="person-firstname">{person.firstName}</div>
            <div className="person-lastname">{person.lastName}</div>
          </Link>
          <p className="person-occupation">{person.position}</p>
        </div>
      )}
    </div>
  );
}

export default PersonCards;
