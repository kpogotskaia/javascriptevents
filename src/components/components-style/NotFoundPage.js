import React from "react";
import { Link } from "react-router-dom";

import './notFoundPage.css';

export function NotFoundPage() {
  return(
    <div className="section">
      <h1 className="heading">Page not Found</h1>
      <p className="description">
        Looks like you've followed a broken link<br></br>
        or entered a URL that doesn't exist on thit site.
      </p>
      <Link to="/" className="link-back">&#8592; Back to Nome Page</Link>
    </div>
  );
}

export default NotFoundPage;
