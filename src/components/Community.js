import React from 'react';
import { useEffect, useState } from 'react';
import PersonCards from './PersonCards';
import { getPersons } from '../services/service-persons';

function Community({urlSegment}) {

  const [persons, setPersons] = useState([]);
  const [isSubscribed, setIsSubscribed] = useState(false);

  useEffect(() => {
    getPersons().then(setPersons);
  }, []);

return (
    <section className="app-section app-section__community js-persones-section">
      <h2 className="app-title">
        Big Community of
        People Like You
      </h2>
      <button className="app-section__button--read-more"
        onClick={e => setIsSubscribed(!isSubscribed)}
      >Hide Section</button>
      <p className="app-subtitle app-section__community-pharagraph">
        We`re proud of our products,
        and we`re really excited
        when we get feedback from our users.
      </p>
      <PersonCards className={isSubscribed ? 'hidden' : 'show'} persons={persons} urlSegment={urlSegment} />
    </section>
  );
}

export default Community;
