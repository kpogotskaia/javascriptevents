function PersonCard({person}) {
  return (
    <div>
      <div key={person.id} className="person">
        <img className="person-img" alt="person avatar" src={person.avatar}/>
        <h4 className="person-description">{person.id}</h4>
          <div className="person-firstname">{person.firstName}</div>
          <div className="person-lastname">{person.lastName}</div>
        <p className="person-occupation">{person.position}</p>
      </div>
    </div>
  );
}

export default PersonCard;
