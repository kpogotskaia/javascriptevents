function HeadlineSection() {
    return (
        <section className="app-section">
        <h2 className="app-title">
          This is the Section Headline,<br /> Continues to Two Lines
        </h2>
        <h3 className="app-subtitle">
          Lorem ipsum dolor sit amet, consectetur<br /> adipiscing elit sed do eiusmod.
        </h3>

        <article className="app-section__article">
          <p>
            Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
            in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis.
          </p>

          <p>
            Minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
            in culpa qui officia deserunt mollit anim id est laborum.
          </p>
        </article>

        <button className="app-section__button app-section__button--read-more">
          Read more
        </button>
      </section>
    );
}

export default HeadlineSection;
