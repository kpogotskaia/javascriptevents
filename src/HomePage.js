import Community from './components/Community';
import CultureSection from './components/CultureSection';
import Footer from './components/Footer';
import Header from './components/Header';
import HeadlineSection from './components/HeadlineSection';
import JoinusSection from './components/JoinusSection';
import MainSection from './components/MainSection';

export function HomePage() {
  return (
    <main className="app-container">
      <Header />
      <MainSection />
      <HeadlineSection />
      <CultureSection />
      <Community urlSegment="community" />
      <JoinusSection />
      <Footer />
  </main>
  );
}

export default HomePage;
