const VALID_EMAIL_ENDINGS = ['gmail.com', 'outlook.com', 'yandex.ru'];

// string -> boolean
export function validate(email) {
  return VALID_EMAIL_ENDINGS.includes(email.split('@')[1]);
}

// string -> Promise<boolean>
export function validateAsync(email) {
  return Promise.resolve(validate(email));
}

export function validateWithThrow(email) {
  const isValid = validate(email);
  if (!isValid) {
    throw new Error('Provided email is invalid');
  }

  return isValid;
}

export function validateWithLog(email) {
  const isValid = validate(email);

  // eslint-disable-next-line
  console.log(isValid);

  return isValid;
}
