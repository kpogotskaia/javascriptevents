export const getPersons = () => {
  return fetch('community').then((r) => r.json());
};

export const getPerson = (id) => {
  return fetch(`${id}`).then((r) => r.json());
};
