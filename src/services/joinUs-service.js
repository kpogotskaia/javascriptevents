import { validate } from './email-validator';

const getRequestData = (userEmail) => ({
  method: 'POST',
  body: JSON.stringify({
    email: userEmail,
  }),
  headers: {
    'Content-Type': 'application/json',
  },
});

export const subcribeFormSubmit = (event, setIsSubscribed, setIsSubbmitting) => {
  event.preventDefault();
  const userEmail = new FormData(event.target).get('email');
  if (validate(userEmail)) {
    setIsSubbmitting(true);
    fetch('subscribe', getRequestData(userEmail))
      .then((r) => {
        setIsSubbmitting(false);
        if (r.status === 500) {
          return r.text();
        }
        return r.json();
      })
      .then(((data) => {
        if (typeof data === 'string') {
          throw new Error(data);
        } else if (data.error) {
          alert(data.error);
        } else if (data.success) {
          localStorage.setItem('email', userEmail);
          setIsSubscribed(true);
        }
      }))
      .catch((error) => {
        alert(error.message);
      });
  }
};

// eslint-disable-next-line
export const unSubcribeFormSubmit = (event, setIsSubscribed, setIsSubbmitting) => {
  event.preventDefault();
  setIsSubbmitting(true);
  fetch('unsubscribe', getRequestData(''))
    .then((r) => {
      setIsSubbmitting(false);
      if (r.status === 500) {
        return r.text();
      }
      return r.json();
    })
    .then(((data) => {
      if (typeof data === 'string') {
        throw new Error(data);
      } else if (data.success) {
        localStorage.setItem('email', '');
        setIsSubscribed(false);
      }
    }))
    .catch((error) => {
      alert(error.message);
    });
};
